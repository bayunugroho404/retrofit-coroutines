package com.example.coroutines.repo

import com.example.coroutines.config.Client
import com.example.coroutines.config.Service

/**
 * Created by baay404@gmail.com
 */

class Repository {
    private var services: Service = Client

    suspend fun getTodo(id: Int) = services.getTodo(id)
}