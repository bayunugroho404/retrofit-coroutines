package com.example.coroutines.config

import com.example.coroutines.model.MyModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by baay404@gmail.com
 */

interface Service {
    @GET("/todos/{id}")
    suspend fun getTodo(@Path("id") id: Int): MyModel
}

val Client: Service by lazy {
    Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(Service::class.java)
}