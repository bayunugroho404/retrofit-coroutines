package com.example.coroutines.model

/**
 * Created by baay404@gmail.com
 */

data class MyModel(
    val id: Int = 0,
    val title: String = "",
    val completed: Boolean = false
)